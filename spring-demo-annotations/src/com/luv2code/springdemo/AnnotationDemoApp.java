package com.luv2code.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AnnotationDemoApp
{
    public static void main(String[] args)
    {
        // read Spring cfg file
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext.xml");

        // get the bean from Spring container
        Coach theCoachCoach = context.getBean("tennisCoach", Coach.class);
        TennisCoach theTennisCoachTennisCoach = context.getBean("tennisCoach", TennisCoach.class);
        Coach theCoachTennisCoach = context.getBean("tennisCoach", TennisCoach.class);
//        TennisCoach theTennisCoachCoach = context.getBean("tennisCoach", Coach.class);

        // call a method on the bean

        System.out.println(theCoachCoach.getDailyWorkout());
//        System.out.println(theTennisCoachTennisCoach.getDailyWorkout());
//        System.out.println(theCoachTennisCoach.getDailyWorkout());
//        System.out.println(theTennisCoachCoach.getDailyWorkout());

//        System.out.println(theBoxingCoach.getDailyWorkout());

        System.out.println(theCoachCoach.getDailyFortune());

        SwimCoach theSwimCoach = context.getBean("swimCoach", SwimCoach.class);
        System.out.println(theSwimCoach.getDailyWorkout());
        System.out.println(theSwimCoach.getDailyFortune());
        System.out.println(theSwimCoach.getEmail());
        System.out.println(theSwimCoach.getTeam());

        // Testing BoxingCoach class

        BoxingCoach theBoxingCoach = context.getBean("boxingCoach", BoxingCoach.class);
        System.out.println(theBoxingCoach.getDailyWorkout());
        System.out.println(theBoxingCoach.getDailyFortune());
        System.out.println(theBoxingCoach.getEmail());
        System.out.println(theBoxingCoach.getTeam());

        // close the context
        context.close();
    }
}
