package com.luv2code.springdemo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:sport.properties")
public class BasketballConfig
{

    @Bean
    public Coach basketballCoach()
    {
        Coach tempCoach = new BasketballCoach(harshFortune());

        return tempCoach;
    }

    @Bean
    public FortuneService harshFortune()
    {
        return new HarshFortune();
    }
}
