package com.luv2code.springdemo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class BasketballCoachTest
{
    public static void main(String[] args)
    {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(BasketballConfig.class);

        BasketballCoach theBasketballCoach = context.getBean("basketballCoach", BasketballCoach.class);
        System.out.println(theBasketballCoach.getDailyWorkout());
        System.out.println(theBasketballCoach.getDailyFortune());
        System.out.println(theBasketballCoach.getEmail());
        System.out.println(theBasketballCoach.getTeam());

        System.out.println();

        Coach otherBasketballCoach = context.getBean("basketballCoach", Coach.class);
        System.out.println(otherBasketballCoach.getDailyWorkout());
        System.out.println(otherBasketballCoach.getDailyFortune());
    }
}
