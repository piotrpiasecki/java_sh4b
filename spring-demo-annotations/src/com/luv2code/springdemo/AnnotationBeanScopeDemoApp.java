package com.luv2code.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AnnotationBeanScopeDemoApp
{
    public static void main(String[] args)
    {
        // load String cfg file
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext.xml");

        // retrieve bean from Spring container
        Coach theCoach = context.getBean("boxingCoach", Coach.class);
        Coach alphaCoach = context.getBean("boxingCoach", Coach.class);

        // check if they are the same
        boolean result = (theCoach == alphaCoach);

        // print out results
        System.out.println("Pointing to the same object: " + result);
        System.out.println("Memory location for theCoach: " + theCoach);
        System.out.println("Memory location for alphaCoach: " + alphaCoach);

        System.out.println(theCoach.getDailyFortune());

        // close the context
        context.close();
    }
}
