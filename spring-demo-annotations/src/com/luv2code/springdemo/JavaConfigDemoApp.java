package com.luv2code.springdemo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class JavaConfigDemoApp
{
    public static void main(String[] args)
    {
        // read Spring cfg class
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SportConfig.class);

        // get the bean from Spring container
        Coach theCoachCoach = context.getBean("tennisCoach", Coach.class);
        TennisCoach theTennisCoachTennisCoach = context.getBean("tennisCoach", TennisCoach.class);
        Coach theCoachTennisCoach = context.getBean("tennisCoach", TennisCoach.class);
//        TennisCoach theTennisCoachCoach = context.getBean("tennisCoach", Coach.class);

        // call a method on the bean

        System.out.println(theCoachCoach.getDailyWorkout());
//        System.out.println(theTennisCoachTennisCoach.getDailyWorkout());
//        System.out.println(theCoachTennisCoach.getDailyWorkout());
//        System.out.println(theTennisCoachCoach.getDailyWorkout());

//        System.out.println(theBoxingCoach.getDailyWorkout());

        System.out.println(theCoachCoach.getDailyFortune());

        // Testing SwimCoach class

        SwimCoach theSwimCoach = context.getBean("swimCoach", SwimCoach.class);
        System.out.println(theSwimCoach.getDailyWorkout());
        System.out.println(theSwimCoach.getDailyFortune());
        System.out.println(theSwimCoach.getEmail());
        System.out.println(theSwimCoach.getTeam());

        // Testing BoxingCoach class

        BoxingCoach theBoxingCoach = context.getBean("boxingCoach", BoxingCoach.class);
        System.out.println(theBoxingCoach.getDailyWorkout());
        System.out.println(theBoxingCoach.getDailyFortune());
        System.out.println(theBoxingCoach.getEmail());
        System.out.println(theBoxingCoach.getTeam());

        // Testing KarateCoach class

        KarateCoach theKarateCoach = context.getBean("karateCoach", KarateCoach.class);
        System.out.println(theKarateCoach.getDailyWorkout());
        System.out.println(theKarateCoach.getDailyFortune());
        System.out.println(theKarateCoach.getEmail());
        System.out.println(theKarateCoach.getTeam());

        // close the context
        context.close();
    }
}
