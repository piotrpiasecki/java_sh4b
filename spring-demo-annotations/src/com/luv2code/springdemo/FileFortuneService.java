package com.luv2code.springdemo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Component
public class FileFortuneService implements FortuneService
{

    private Random myRandom = new Random();


/*
    private String[] data;

    @Value("${foo.fortune1}")
    private String fortune1;

    @Value("${foo.fortune2}")
    private String fortune2;

    @Value("${foo.fortune3}")
    private String fortune3;



    @PostConstruct                  //   Necessary to add '@PostConstruct' because of bean lifecycle!
    public void setupMyData()
    {
        data = new String[3];       //   C R U C I A L ! ! !
        data[0] = fortune1;
        data[1] = fortune2;
        data[2] = fortune3;
    }*/

    private List<String> fortunes;

    public FileFortuneService()
    {
        System.out.println(">> Inside default constructor method.");
    }

    @PostConstruct
    private void loadFortunesFile()
    {
        String fileAddress = "C:\\Users\\piase\\Desktop\\Programowanie\\java_SH4B\\" +
                "spring-demo-annotations\\src\\com\\luv2code\\springdemo\\fortunes_file.txt";

        fortunes = new ArrayList<>();
        File file = new File(fileAddress);

        System.out.println(">> FileFortuneService: inside method loadTheFortunesFile");
        System.out.println("Loading fortunes from " + file);
        System.out.printf("The file %s%n", file.exists() ? "exists" : "does not exist");

        try (BufferedReader reader = new BufferedReader(new FileReader(file)))
        {
            String tempLine;

            while ((tempLine = reader.readLine()) != null)
            {
                fortunes.add(tempLine);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public String getFortune()
    {
        return fortunes.get(myRandom.nextInt(fortunes.size()));
    }
}
