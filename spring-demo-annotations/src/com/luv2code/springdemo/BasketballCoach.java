package com.luv2code.springdemo;

import org.springframework.beans.factory.annotation.Value;

public class BasketballCoach implements Coach
{
    private FortuneService fortuneService;

    public BasketballCoach(FortuneService fortuneService)
    {
        this.fortuneService = fortuneService;
    }

    @Value("${foo.email}")
    private String email;

    @Value("${foo.team}")
    private String team;

    @Override
    public String getDailyWorkout()
    {
        return "Practice throwing for one hour.";
    }

    @Override
    public String getDailyFortune()
    {
        return fortuneService.getFortune();
    }

    public String getEmail()
    {
        return email;
    }

    public String getTeam()
    {
        return team;
    }
}
