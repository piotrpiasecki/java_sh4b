package com.luv2code.springdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
//@Scope("prototype")
public class TennisCoach implements Coach
{
    @Autowired
    @Qualifier("randomFortuneService")
    private FortuneService fortuneService;

    // define init method
    @PostConstruct
    public void doMyStartupStuff()
    {
        System.out.println(">> TennisCoach: inside doMyStartupStuff method");
    }

    // define destroy method
    @PreDestroy
    public void doMyCleanupStuff()
    {
        System.out.println(">> TennisCoach: inside doMyCleanupStuff method");
    }


//    @Autowired
//    public TennisCoach(FortuneService fortuneService)
//    {
//        this.fortuneService = fortuneService;
//    }

    public TennisCoach() { System.out.println("Inside default constructor"); }

//    Setter method with @Autowired annotation

//    @Autowired
//    public void setFortuneService(FortuneService fortuneService)
//    {
//        System.out.println("Inside setter method");
//        this.fortuneService = fortuneService;
//    }

    @Override
    public String getDailyWorkout()
    {
        return "Practice your backhand volley";
    }

    @Override
    public String getDailyFortune()
    {
        return fortuneService.getFortune();
    }
}
