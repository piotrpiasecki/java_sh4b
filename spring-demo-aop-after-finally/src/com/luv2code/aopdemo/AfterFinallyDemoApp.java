package com.luv2code.aopdemo;

import com.luv2code.aopdemo.dao.AccountDAO;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;

public class AfterFinallyDemoApp
{
    public static void main(String[] args)
    {
        // read spring cfg java class
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(DemoConfig.class);

        // get the account bean from spring container
        AccountDAO theAccountDAO = context.getBean("accountDAO", AccountDAO.class);

        // call the method to find the accounts

        List<Account> theAccounts = null;

        try
        {
            // add a boolean flag to simulate exceptions
            boolean tripWire = false;
            theAccounts = theAccountDAO.findAccounts(tripWire);
        }
        catch (Exception e)
        {
            System.out.println("\n\nMain program...caught exception: " + e);
        }

        // display the accounts

        System.out.println("\n\nMain program: AfterFinallyDemoApp");
        System.out.println("-------");

        System.out.println(theAccounts);



        // close the context
        context.close();
    }
}
