<%--
  Created by IntelliJ IDEA.
  User: piase
  Date: 21.09.2019
  Time: 16:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>

<head>
    <title>List Customers</title>

    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css"/>
</head>

<body>

<div id="wrapper">
    <div id="header">
        <h2>CRM - Customer Relationship Manager</h2>
    </div>
</div>

<div id="container">
    <div id="content">

        <%-- putting new button: Add customer --%>

        <input type="button" value="Add Customer"
               onclick="window.location.href = 'showFormForAdd'; return false;"
               class="add-button"
        />

        <form:form action="search" method="GET">
            Search customer: <input type="text" name="theSearchName"/>

            <input type="submit" value="Search" class="add-button"/>

        </form:form>

        <%-- add aout a html table here --%>

        <table>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Action</th>
            </tr>

            <%-- loop over and print out customers --%>

            <c:forEach var="tempCustomer" items="${customers}">

                <%-- contruct an "update" link with customer id --%>

                <c:url var="updateLink" value="/customer/showFormForUpdate">
                    <c:param name="customerId" value="${tempCustomer.id}"/>
                </c:url>

                <%-- contruct a "delete" link with customer id --%>

                <c:url var="deleteLink" value="/customer/delete">
                    <c:param name="customerId" value="${tempCustomer.id}"/>
                </c:url>

                <tr>
                    <td>${tempCustomer.firstName}</td>
                    <td>${tempCustomer.lastName}</td>
                    <td>${tempCustomer.email}</td>

                    <td>
                            <%-- display the update link --%>
                        <a href="${updateLink}">Update</a>
                        |
                            <%-- 'confirm()' displays a confirmation popup dialog--%>
                        <a href="${deleteLink}"
                           onclick="if (!(confirm('Are you sure you want to delete this customer?'))) return false">Delete</a>

                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>
</div>

</body>

</html>
