package com.luv2code.springdemo.controller;

import com.luv2code.springdemo.entity.Customer;
import com.luv2code.springdemo.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/customer")
public class CustomerController
{
    // need to inject the customer service
    @Autowired
    private CustomerService customerService;
    private int theId;
    private Model theModel;

    //    @RequestMapping("/list")
    @GetMapping("/list")
    public String listCustomers(Model theModel)
    {
        // get customers from service
        List<Customer> theCustomers = customerService.getCustomers();

        // add the customers to the model
        theModel.addAttribute("customers", theCustomers);
        return "list-customers";
    }

    @GetMapping("/showFormForAdd")
    public String showFormForAdd(Model theModel)
    {
        // create model attribute to bind form data
        Customer theCustomer = new Customer();

        // add the customer to the model
        theModel.addAttribute("customer", theCustomer);
        return "customer-form";
    }

    @PostMapping("/saveCustomer")
    public String saveCustomer(@ModelAttribute("customer") Customer theCustomer)
    {
        // save the customer using the service
        customerService.saveCustomer(theCustomer);
        return "redirect:/customer/list";
    }

    @GetMapping("/showFormForUpdate")
    public String showFromForUpdate(@RequestParam("customerId") int theId, Model theModel)
    {
        this.theId = theId;
        this.theModel = theModel;
        // get the customer from the db

        Customer theCustomer = customerService.getCustomer(theId);

        // set customer as a model attribute to pre-populate the form

        theModel.addAttribute("customer", theCustomer);

        // send over to the form

        return "customer-form";
    }

    @GetMapping("/delete")
    public String delete(@RequestParam("customerId") int theId, Model theModel)
    {
        this.theId = theId;
        this.theModel = theModel;
        // get the customer from the db

        customerService.deleteCustomer(theId);

        return "redirect:/customer/list";
    }

    @GetMapping("/search")
    public String searchCustomers(@RequestParam("theSearchName") String theSearchName, Model theModel)
    {
        List<Customer> theCustomers =
                customerService.searchCustomers(theSearchName);

        theModel.addAttribute("customers",theCustomers);

        return "list-customers";
    }
}
