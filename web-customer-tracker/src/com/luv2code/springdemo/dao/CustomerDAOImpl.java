package com.luv2code.springdemo.dao;

import com.luv2code.springdemo.entity.Customer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CustomerDAOImpl implements CustomerDAO
{
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Customer> getCustomers()
    {
        // get the current hibernate session
        Session currentSession = sessionFactory.getCurrentSession();

        // create query + sort by last name
        Query<Customer> theQuery =
                currentSession.createQuery("from Customer order by lastName", Customer.class);

        // execute query and get result list
        List<Customer> customers = theQuery.getResultList();

        // return the results
        return customers;
    }

    @Override
    public void saveCustomer(Customer theCustomer)
    {
        // get the current hibernate session
        Session currentSession = sessionFactory.getCurrentSession();

        // save the customer to the db
        currentSession.saveOrUpdate(theCustomer);
    }

    @Override
    public Customer getCustomer(int theId)
    {
        Session currentSession = sessionFactory.getCurrentSession();

        Customer theCustomer = currentSession.get(Customer.class, theId);

        return theCustomer;
    }

    @Override
    public void deleteCustomer(int theId)
    {
        Session currentSession = sessionFactory.getCurrentSession();

        Customer theCustomer = currentSession.get(Customer.class, theId);

        currentSession.delete(theCustomer);

//        Query theQuery = currentSession.createQuery("delete from Customer where id=:customerId");
//        theQuery.setParameter("customerId", theId);
//        theQuery.executeUpdate();
    }

    /**
     * In this method, we need to check "theSearchName",
     * this is the user input. We need to make sure it is not empty.
     * If it is not empty then we will use it in the search query.
     * If it is empty, then we'll just ignore it
     * and simply return all of the customers.
     *
     * For the condition when "theSearchName" is not empty,
     * then we use it to compare against the first name or last name.
     *
     * We also make use of the "like" clause and the "%" wildcard characters.
     * This will allow us to search for substrings. For example,
     * if we have customers with last name of "Patel", "Patterson" ... then we can search for "Pat"
     * and it will match on those names.
     *
     * Also, notice the query uses the lower case version of the values to make a case insensitive search.
     * If you'd like to make a case sensitive search, then simply remove the lower references.
     * @param theSearchName
     * @return
     */

    @Override
    public List<Customer> searchCustomers(String theSearchName)
    {
        Session currentSession = sessionFactory.getCurrentSession();

        Query theQuery = null;

        // only search by name if theSearchName is not empty

        if (theSearchName != null && theSearchName.trim().length() > 0)
        {
            // search for firstName or lastName... case insensitive

            theQuery = currentSession.createQuery(
                    "from Customer where lower(firstName) like :theName or lower(lastName) like :theName",
                    Customer.class);

            theQuery.setParameter("theName", "%" + theSearchName.toLowerCase() + "%");
        }
        else
        {
            // theSearchName is empty... so just get all customers
            theQuery = currentSession.createQuery("from Customer", Customer.class);
        }

        return theQuery.getResultList();
    }
}
