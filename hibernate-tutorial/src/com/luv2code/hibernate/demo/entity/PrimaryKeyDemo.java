package com.luv2code.hibernate.demo.entity;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class PrimaryKeyDemo
{
    public static void main(String[] args)
    {
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();

        Session session = factory.getCurrentSession();

        try
        {
            // using session object to save/retrieve Java objects

            System.out.println("Creating new Student object...");

            Student tempStudent1 = new Student("John", "Doe", "john_doe@luv2code.com");
            Student tempStudent2 = new Student("Mary", "Public", "mary_public@luv2code.com");
            Student tempStudent3 = new Student("Bonita", "Applebaum", "bonita_applebaum@luv2code.com");
            session.beginTransaction();

            System.out.println("Saving the Student object...");
            session.save(tempStudent1);
            session.save(tempStudent2);
            session.save(tempStudent3);

            session.getTransaction().commit();

            System.out.println("Done!");
        }
        finally
        {
            factory.close();
        }

    }
}
