package com.luv2code.hibernate.demo.entity;

import org.apache.commons.lang3.time.DateUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CreateStudentDemo
{
    public static void main(String[] args) throws ParseException
    {
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();

        Session session = factory.getCurrentSession();

        try
        {
            // using session object to save/retrieve Java objects

            System.out.println("Creating new Student object...");

            Student tempStudent = new Student("John", "Doe", "john_doe@luv2code.com");
            String marysDateOfBirthString = "31-12-1998";
            Date marysDateOfBirth = DateUtils.parseDate(marysDateOfBirthString,"dd-MM-yyyy");
            Student tempStudent2 =
                    new Student("Mary", "Bloody", "mary_bloody@luv2code.com", marysDateOfBirth);
            session.beginTransaction();

            System.out.println("Saving the Student object...");
            session.save(tempStudent);
            session.save(tempStudent2);

            session.getTransaction().commit();

            System.out.println("Done!");
        }
        finally
        {
            factory.close();
        }

    }
}