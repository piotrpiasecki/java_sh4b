package com.luv2code.hibernate.demo.entity;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class UptadeStudentDemo
{
    public static void main(String[] args)
    {
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();

        Session session = factory.getCurrentSession();

        try
        {

            int studentId = 1;

            session.beginTransaction();

            System.out.println("\nGetting student with Id: " + studentId);

            Student myStudent = session.get(Student.class, studentId);

            System.out.println("Updating student : " + myStudent);

            myStudent.setFirstName("Scooby");

            session.getTransaction().commit();

            session = factory.getCurrentSession();
            session.beginTransaction();

            System.out.println("Updating email for all students.");

            session.createQuery("UPDATE Student SET email = 'foo@gmail.com'").executeUpdate();

            session.getTransaction().commit();

            System.out.println("Done!");
        }
        finally
        {
            factory.close();
        }

    }
}