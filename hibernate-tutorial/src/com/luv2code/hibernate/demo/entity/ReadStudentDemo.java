package com.luv2code.hibernate.demo.entity;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class ReadStudentDemo
{
    public static void main(String[] args)
    {
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();

        Session session = factory.getCurrentSession();

        try
        {
            // using session object to save/retrieve Java objects

            System.out.println("Creating new Student object...");

            Student tempStudent = new Student("Daffy", "Duck", "daffy_duck@luv2code.com");
            session.beginTransaction();

            System.out.println("Saving the Student object...");
            session.save(tempStudent);

            session.getTransaction().commit();

            System.out.println("Saved student. Generated id: " + tempStudent.getId());

            session = factory.getCurrentSession();
            session.beginTransaction();

            System.out.println("\n\nGetting student with id: " + tempStudent.getId());

            Student myNewStudent = session.get(Student.class, tempStudent.getId());

            System.out.println("Get complete: " + myNewStudent);

            session.getTransaction().commit();

            System.out.println("Done!");
        }
        finally
        {
            factory.close();
        }

    }
}