package com.luv2code.hibernate.demo.entity;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DeleteStudentDemo
{
    public static void main(String[] args)
    {
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();



        try
        {
            int studentId = 1;

            Session session = factory.getCurrentSession();
            session.beginTransaction();
//
//            System.out.println("\nGetting student with Id: " + studentId);
//
//            Student myStudent = session.get(Student.class, studentId);
//
//            System.out.println("\nDeleting student with Id: " + studentId);
//
//            session.delete(myStudent);

//            session.getTransaction().commit();

            System.out.println("\nDeleting student with ID = 2");

            session.createQuery("DELETE FROM Student WHERE id = 2").executeUpdate();

            session.getTransaction().commit();

            System.out.println("Done!");
        }
        finally
        {
            factory.close();
        }

    }
}