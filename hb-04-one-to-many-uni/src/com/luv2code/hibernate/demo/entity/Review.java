package com.luv2code.hibernate.demo.entity;

import javax.persistence.*;

@Entity
@Table(name = "review")
public class Review
{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "comment")
    private String comment;

//    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
//    @Column(name = "course_id")
//    private int courseId;

    public Review()
    {
    }

    public Review(String comment)
    {
        this.comment = comment;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

//    public int getCourseId()
//    {
//        return courseId;
//    }
//
//    public void setCourseId(int courseId)
//    {
//        this.courseId = courseId;
//    }

    @Override
    public String toString()
    {
        return "Review{" +
                "id=" + id +
                ", comment='" + comment + '\'' +
//                ", courseId=" + courseId +
                '}';
    }
}
