package com.luv2code.hibernate.demo;

import com.luv2code.hibernate.demo.entity.Instructor;
import com.luv2code.hibernate.demo.entity.InstructorDetail;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DeleteDemo
{
    public static void main(String[] args)
    {
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .buildSessionFactory();

        Session session = factory.getCurrentSession();

        try
        {
            session.beginTransaction();

            int theId = 1;

            Instructor tempInstructor = session.get(Instructor.class, theId);

            System.out.println("Found instructor: " + tempInstructor);

            if (tempInstructor != null)
            {
                System.out.println("Deleting:" + tempInstructor);

                // NOTE: will ALSO delete associated "details" object
                // because of CascadeType.ALL
                session.delete(tempInstructor);
            }
            else
            {
                System.out.println("Instructor with id " + theId + " is null, therefore won't be .");
            }

            session.getTransaction().commit();
        }
        finally
        {
            factory.close();
        }
    }
}
