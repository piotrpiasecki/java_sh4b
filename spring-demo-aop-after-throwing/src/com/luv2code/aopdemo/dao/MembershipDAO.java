package com.luv2code.aopdemo.dao;

import org.springframework.stereotype.Component;

@Component
public class MembershipDAO
{
    private String name;

    public boolean addMember()
    {
        System.out.println(getClass() + ": DOING STUFF: ADDING A MEMBERSHIP ACCOUNT");
        return true;
    }

    public void goToSleep()
    {
        System.out.println(getClass() + ": going to sleep");
    }

    public String getName()
    {
        System.out.println("Inside getter");
        return name;
    }

    public void setName(String name)
    {
        System.out.println("Inside setter");
        this.name = name;
    }
}
