package com.luv2code.aopdemo.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Aspect
@Order(3)
@Component
public class MyApiAnalyticsAspect
{
    @Before("com.luv2code.aopdemo.aspect.LuvAopExpressions.forDaoPackageNotGetterNorSetter()")
    public void performApiAnalytics()
    {
        System.out.println("====>> Performing API analytics");
    }
}
