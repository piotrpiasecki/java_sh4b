package com.luv2code.aopdemo;

import com.luv2code.aopdemo.dao.AccountDAO;
import com.luv2code.aopdemo.dao.MembershipDAO;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;

public class AfterReturningDemoApp
{
    public static void main(String[] args)
    {
        // read spring cfg java class
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(DemoConfig.class);

        // get the account bean from spring container
        AccountDAO theAccountDAO = context.getBean("accountDAO", AccountDAO.class);

        // call the method to find the accounts

        List<Account> theAccounts = theAccountDAO.findAccounts();

        // display the accounts

        System.out.println("\n\nMain program: AfterReturningDemoApp");
        System.out.println("-------");

        System.out.println(theAccounts);



        // close the context
        context.close();
    }
}
