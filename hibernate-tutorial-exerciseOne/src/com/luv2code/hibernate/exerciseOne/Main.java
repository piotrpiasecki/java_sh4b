package com.luv2code.hibernate.exerciseOne;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import static com.luv2code.hibernate.exerciseOne.SaveEmployee.*;
import static com.luv2code.hibernate.exerciseOne.ReadEmployee.*;
import static com.luv2code.hibernate.exerciseOne.UpdateEmployee.*;
import static com.luv2code.hibernate.exerciseOne.FindEmployee.*;
import static com.luv2code.hibernate.exerciseOne.DeleteEmployee.*;

public class Main
{
    public static void main(String[] args)
    {
        SessionFactory factory = null;
        try
        {
            factory = new Configuration()
                    .configure("hibernate.cfg.xml")
                    .addAnnotatedClass(Employee.class)
                    .buildSessionFactory();

            Session session = factory.openSession();
            session.beginTransaction();
            session.createSQLQuery("DELETE FROM Employee").executeUpdate();
            session.createSQLQuery("ALTER TABLE Employee AUTO_INCREMENT = 1").executeUpdate();
            session.getTransaction().commit();
            Employee employee1 = new Employee("John", "Nowak", "Ford");
            Employee employee2 = new Employee("Mary", "Denver", "Sony");
            Employee employee3 = new Employee("Bob", "Ginger", "Pepsi");

            System.out.println("Employee1: ");
            displayEmployee(employee1);
            System.out.println("Employee2: ");
            displayEmployee(employee2);
            System.out.println("Employee3: ");
            displayEmployee(employee3);

            saveEmployee(factory, employee1);
            saveEmployee(factory, employee2);
            saveEmployee(factory, employee3);
            System.out.println("Save done!");

            int sampleID = employee1.getId();
            Employee employee4 = readEmployee(factory, sampleID);
            saveEmployee(factory, employee4);
            System.out.println("Retrieval completed!");

            System.out.println("Employee4: ");
            displayEmployee(employee4);

            employee2 = updateEmployee(factory, employee2.getId(), "Maryanne", "Passata", "Samsung");
            System.out.println("Update completed!");
            displayEmployee(employee2);

            employee3 = updateEmployee(factory, employee3.getId(),"Adam", null, null);
            System.out.println("Update completed!");
            displayEmployee(employee3);

            deleteEmployee(factory,  4);
            System.out.println("Delete completed!");
            displayEmployee(readEmployee(factory,4));

            for (Employee employee : findEmployee(factory, "Nike"))
            {
                displayEmployee(employee);
            }

            for (Employee employee : findEmployee(factory, "Ford"))
            {
                displayEmployee(employee);
            }

        }
        finally
        {
            if (factory != null) { factory.close(); }
        }
    }

    public static void displayEmployee(Employee employee)
    {
        System.out.println(employee);
    }
}
