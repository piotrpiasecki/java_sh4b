package com.luv2code.hibernate.exerciseOne;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class FindEmployee
{
    public static List<Employee> findEmployee(SessionFactory sessionFactory, String company)
    {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        List<Employee> employees =
                (List<Employee>) session.createQuery("FROM Employee e WHERE e.company LIKE '" + company + "'").getResultList();

        for (Employee employee  : employees)
        {
            System.out.println(employee);
        }

        session.getTransaction().commit();

        return employees;
    }
}
