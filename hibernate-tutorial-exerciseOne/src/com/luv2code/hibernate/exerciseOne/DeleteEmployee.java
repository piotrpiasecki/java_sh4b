package com.luv2code.hibernate.exerciseOne;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class DeleteEmployee
{
    public static Employee deleteEmployee(
            SessionFactory sessionFactory, int id)
    {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        Employee tempEmployee = session.get(Employee.class, id);
        session.delete(tempEmployee);

        session.getTransaction().commit();

        System.out.println("Deleted employee: " + tempEmployee);

        return tempEmployee;
    }
}
