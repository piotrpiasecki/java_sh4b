package com.luv2code.hibernate.exerciseOne;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class ReadEmployee
{
    public static Employee readEmployee(SessionFactory sessionFactory, int id)
    {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        Employee tempEmployee = session.get(Employee.class,id);
        session.getTransaction().commit();

        System.out.printf("Reading employee: %s%n", tempEmployee);

        return tempEmployee;
    }


}
