package com.luv2code.hibernate.exerciseOne;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class SaveEmployee
{
    public static Employee saveEmployee(SessionFactory sessionFactory, Employee employee)
    {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.save(employee);
        session.getTransaction().commit();

        System.out.printf("Saving employee: %s%n", employee);

        return employee;
    }


}
