package com.luv2code.hibernate.exerciseOne;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.executable.ValidateOnExecution;

public class UpdateEmployee
{
    public static Employee updateEmployee(
            SessionFactory sessionFactory, int id, String firstName, String lastName, String company)
    {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        Employee tempEmployee = session.get(Employee.class,id);

        System.out.println("Updating employee: " + tempEmployee);

        if (firstName != null) { tempEmployee.setFirstName(firstName); }
        if (lastName != null) { tempEmployee.setLastName(lastName); }
        if (company != null) { tempEmployee.setCompany(company); }

        session.getTransaction().commit();

        System.out.println("Employee after update: " + tempEmployee);

        return tempEmployee;
    }

    public static Employee updateEmployee(
            SessionFactory sessionFactory, int id, String firstName, String lastName)
    {
        return UpdateEmployee.updateEmployee(
                sessionFactory, id, firstName, lastName, null);

    }

    public static Employee updateEmployee(
            SessionFactory sessionFactory, int id, String firstName)
    {
        return UpdateEmployee.updateEmployee(
                sessionFactory, id, firstName, null, null);
    }
}
