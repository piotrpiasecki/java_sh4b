package com.luv2code.hibernate.demo;

import com.luv2code.hibernate.demo.entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DeletePacmanCourseDemo
{
    public static void main(String[] args)
    {
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .addAnnotatedClass(Review.class)
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();

        Session session = factory.getCurrentSession();

        try
        {
            session.beginTransaction();

            // get the Pacman course from db
            int theId = 1;
            Course tempCourse = session.get(Course.class, theId);

            System.out.println("\nGot course " + tempCourse);

            System.out.println("Deleting course " + tempCourse);

            session.delete(tempCourse);
            
            session.getTransaction().commit();

            System.out.println("Done!");
        }
        finally
        {
            session.close();
            factory.close();
        }
    }
}
