package com.luv2code.aopdemo.aspect;

import com.luv2code.aopdemo.AroundWithLoggerDemoApp;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Aspect
@Order(1)
@Component
public class MyCloudLogAspect
{
    private static Logger myLogger =
            Logger.getLogger(AroundWithLoggerDemoApp.class.getName());

    @Before("com.luv2code.aopdemo.aspect.LuvAopExpressions.forDaoPackageNotGetterNorSetter()")
    public void logToCloudAsync()
    {
        myLogger.info("\n====>> Logging to Cloud in async fashion");
    }
}
