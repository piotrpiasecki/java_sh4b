package com.luv2code.aopdemo.aspect;

import com.luv2code.aopdemo.Account;
import com.luv2code.aopdemo.AroundWithLoggerDemoApp;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.logging.Logger;

@Aspect
@Order(2)
@Component
public class MyDemoLoggingAspect
{
    private static Logger myLogger =
            Logger.getLogger(AroundWithLoggerDemoApp.class.getName());

    @Around("execution(* com.luv2code.aopdemo.service.TrafficFortuneService.getFortune(..))")
    public Object aroundGetFortune(ProceedingJoinPoint proceedingJoinPoint) throws Throwable
    {
        // print out the method we are advising on

        String method = proceedingJoinPoint.getSignature().toShortString();
        myLogger.info("\n=====>>> Executing @Around on method: " + method);

        // get begin timestamp
        long start = System.currentTimeMillis();

        // now execute the method
        Object result = proceedingJoinPoint.proceed();

        // get end timestamp
        long stop = System.currentTimeMillis();

        // compute duration and display it
        long duration = stop - start;
        myLogger.info("\n=====>>> Duration: " + duration / 1000.0 + " seconds");

        return result;
    }

    // add a new advice for @AfterFinally on the findAccounts method

    @After("execution(* com.luv2code.aopdemo.dao.AccountDAO.findAccounts(..))")
    public void afterFinallyFindAccountsAdvice(JoinPoint joinPoint)
    {
        String method = joinPoint.getSignature().toShortString();
        myLogger.info("=====>>> Executing @After on method: " + method);
    }

    // add a new advice for @AfterThrowing on the findAccounts method

    @AfterThrowing(
            pointcut = "execution(* com.luv2code.aopdemo.dao.AccountDAO.findAccounts(..))",
            throwing = "e"
    )
    public void afterThrowingFindAccountsAdvice(JoinPoint joinPoint, Throwable e)
    {
        // print out which method we are advising on
        String method = joinPoint.getSignature().toShortString();
        myLogger.info("\n=====>>> Executing @AfterThrowing on method: " + method);

        // print out the results of the method call

        myLogger.info("\n=====>>> The exception is: " + e);
    }

    // add a new advice for @AfterReturning on the findAccounts method

    @AfterReturning(
            pointcut = "execution(* com.luv2code.aopdemo.dao.AccountDAO.findAccounts(..))",
            returning = "result")
    public void afterReturningFindAccountsAdvice(JoinPoint joinPoint, List<Account> result)
    {
        // print out which method we are advising on
        String method = joinPoint.getSignature().toShortString();
        myLogger.info("\n=====>>> Executing @AfterReturning on method: " + method);

        // print out the results of the method call

        myLogger.info("\n=====>>> The result is: " + result);

        // let's post-process the data... let's modify it a bit

        // convert the account names to uppercase
        convertAccountNamesToUpperCase(result);

        myLogger.info("\n=====>>> The result after modification is: " + result);

    }

    private static void convertAccountNamesToUpperCase(List<Account> result)
    {
        for (Account account : result)
        {
            account.setName(account.getName().toUpperCase());
        }
    }


    @Before("com.luv2code.aopdemo.aspect.LuvAopExpressions.forDaoPackage()")
    public void beforeAddAccountAdvice(JoinPoint theJoinPoint)
    {
        myLogger.info("\n====>> Executing @Before advice on addAccount()");

        // display the method signature
        MethodSignature theMethodSignature = (MethodSignature) theJoinPoint.getSignature();
        myLogger.info("Method: " + theMethodSignature);

        // display the method arguments
        Object[] methodArgs = theJoinPoint.getArgs();

        for (Object arg : methodArgs)
        {
            myLogger.info("Argument: " + arg);

            if (arg instanceof Account)
            {
                Account account = (Account) arg;

                myLogger.info("Account name: " + account.getName());
                myLogger.info("Account level: " + account.getLevel());
            }
        }
    }
}
