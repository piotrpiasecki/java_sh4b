package com.luv2code.aopdemo.dao;

import com.luv2code.aopdemo.Account;
import com.luv2code.aopdemo.AroundWithLoggerDemoApp;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Component
public class AccountDAO
{
    private static Logger myLogger =
            Logger.getLogger(AroundWithLoggerDemoApp.class.getName());

    private String name;
    private String serviceCode;

    public void addAccount(Account theAccount, boolean vipFlag)
    {
        System.out.println(getClass() + ": DOING MY DB WORK: ADDING AN ACCOUNT");
    }

    public void doWork()
    {
        System.out.println(getClass() + ": doWork()");
    }

    // add a new method: findAccounts()

    public List<Account> findAccounts(boolean tripWire)
    {
        // simulate exception

        if (tripWire)
        {
            throw new RuntimeException("No soup for You!");
        }

        List<Account> myAccounts = new ArrayList<>();

        // create sample accounts

        Account temp1 = new Account("John", "Silver");
        Account temp2 = new Account("Madhu", "Platinum");
        Account temp3 = new Account("Luca", "Gold");

        // add them to list

        myAccounts.add(temp1);
        myAccounts.add(temp2);
        myAccounts.add(temp3);

        return myAccounts;
    }

}
