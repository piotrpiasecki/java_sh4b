package com.luv2code.aopdemo.dao;

import com.luv2code.aopdemo.AroundWithLoggerDemoApp;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
public class MembershipDAO
{
    private static Logger myLogger =
            Logger.getLogger(AroundWithLoggerDemoApp.class.getName());

    private String name;

    public boolean addMember()
    {
        myLogger.info(getClass() + ": DOING STUFF: ADDING A MEMBERSHIP ACCOUNT");
        return true;
    }

    public void goToSleep()
    {
        System.out.println(getClass() + ": going to sleep");
    }

    public String getName()
    {
        System.out.println("Inside getter");
        return name;
    }

    public void setName(String name)
    {
        System.out.println("Inside setter");
        this.name = name;
    }
}
