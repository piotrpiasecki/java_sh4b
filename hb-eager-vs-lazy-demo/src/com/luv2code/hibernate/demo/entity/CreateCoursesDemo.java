package com.luv2code.hibernate.demo.entity;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class CreateCoursesDemo
{
    public static void main(String[] args)
    {
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .buildSessionFactory();

        Session session = factory.getCurrentSession();

        try
        {
            session.beginTransaction();

            // get the instructor from db

            int theId = 1;
            Instructor tempInstructor = session.get(Instructor.class, theId);

            // create some courses

            Course course1 = new Course("Air Guitar - The Ultimate Guide");
            Course course2 = new Course("The Pinball Masterclass");

            // add courses to instructor

            tempInstructor.add(course1);
            tempInstructor.add(course2);

            // save courses

            session.save(course1);
            session.save(course2);

            session.getTransaction().commit();
        }
        finally
        {
            session.close();
            factory.close();
        }
    }
}
