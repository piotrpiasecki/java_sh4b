package com.luv2code.hibernate.demo.entity;

import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class FetchJoinDemo
{
    public static void main(String[] args)
    {
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .buildSessionFactory();

        Session session = factory.getCurrentSession();

        try
        {
            session.beginTransaction();

            // get the instructor from db

            int theId = 1;

            // Option 2: use HQL

            //noinspection JpaQlInspection
            Query<Instructor> query =
                    session.createQuery("select i from Instructor i " +
                                "JOIN FETCH i.courses " +
                                "where i.id=:theInstructorId", Instructor.class);

            query.setParameter("theInstructorId", theId);

            Instructor tempInstructor = query.getSingleResult();

            System.out.println("luv2code: Instructor: " + tempInstructor);

            session.getTransaction().commit();

            session.close();

            System.out.println("Executed while session is closed!");

            System.out.println("luv2code: Courses: " + tempInstructor.getCourses());
        }
        finally
        {
            session.close();
            factory.close();
        }
    }
}
